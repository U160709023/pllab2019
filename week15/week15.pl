#\(.*) any kind of character can see 0 or more times-----http://httpd.apache.org/docs/2.4/en/logs.html



use strict;
use warnings;


my $filename = $ARGV[0];

open IN, '<', $filename  or die "can'not open $filename!\n";                #first parameter is file handler , < means read , > means write, >> append

my @lines = <IN>;

close IN;

foreach my $line(@lines){ ##dash ch
    chomp $line;
    #print $line, "\n";
    
    if($line =~/^(.*)\s+\-\s\-\s(\[.*\])\s+\".*\"\s+(\d{3})\s+(\d+)/){ #if it is fitting this pattern
                 #$1            #$2                           $3
            my $client = $1;
            my $time  =  $2;
            my $status = $3;
            my $DataSize = $4;
            
    
            print "$client\t$DataSize\t$time\t$status\n";             
        

    }

}


