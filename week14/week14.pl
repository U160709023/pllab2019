use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;
chomp $input;

my $regex = '^\s*$';
$regex = '^[A-Z]+$'; # + means at least one
$regex = '[A-Z]\d*';
$regex = '^\d+\.\d+$'; # ^ controls at the beginning of input
$regex = '^(\d{1,3}\.){3}\d{1,3}$';  #{1.3} means at least one , at least 3 digits

until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}
